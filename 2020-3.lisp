(defun read-file ()
  (with-open-file (s "2020-3-input.txt")
	(loop for line = (read-line s nil)
		  while line
		  collect line)))

(defun make-world ()
  (let* ((lines (read-file)))
	(make-array (list (length lines) (length (car lines)))
				:initial-contents lines)))

(defun traverse (world dx dy)
  (let* ((height (array-dimension world 0))
		 (width (array-dimension world 1)))
	(loop for x from dx by dx
		  for y from dy by dy
		  while (< y height)
		  count (char= #\# (aref world (mod y height) (mod x width))))))

(defun day-3-a ()
  (let* ((world (make-world)))
	(traverse world 3 1)))

(defun day-3-b ()
  (let* ((world (make-world))
		 (slopes '((1 1)
				   (3 1)
				   (5 1)
				   (7 1)
				   (1 2))))
	(reduce #'* (loop for (dx dy) in slopes collect (traverse world dx dy)))))

