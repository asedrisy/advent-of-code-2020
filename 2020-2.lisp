(ql:quickload "cl-ppcre")


(defun part-a ()
  (labels ((valid-password-1-p (line)
			 (cl-ppcre:register-groups-bind
				 (min max character password)
				 ("(\\d{1,2})-(\\d{1,2}) (.): (.+)" line)
			   (let ((n (parse-integer min))
					 (m (parse-integer max))
					 (c (coerce character 'character)))
				 (<= n (count c password) m)))))
  (with-open-file (stream "2020-2-input.txt")
	(loop for line = (read-line stream nil)
		  while line
		  count (valid-password-1-p line)))))
  
(defun part-b ()
  (labels ((valid-password-2-p (line)
			 (cl-ppcre:register-groups-bind
				 (min max character password)
				 ("(\\d{1,2})-(\\d{1,2}) (.): (.+)" line)
			   (let* ((n (1- (parse-integer min)))
					  (m (1- (parse-integer max)))
					  (c (coerce character 'character))
					  (char-pos-1-equal (char= c (char password n)))
					  (char-pos-2-equal (char= c (char password m))))
				 (= 1 (+ (if (char= c (char password n)) 1 0)
						 (if (char= c (char password m)) 1 0)))))))
	(with-open-file (stream "2020-2-input.txt")
	  (loop for line = (read-line stream nil)
			while line
			count (valid-password-2-p line)))))
